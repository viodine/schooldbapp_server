import { sql } from "../db.js";

export class Student {
    constructor({firstName, lastName, pesel, age, gender, email, Classes_IdClasses}) {
        this.firstName = firstName,
        this.lastName = lastName,
        this.pesel = pesel, // TODO
        this.age = age, // TODO
        this.gender = gender, // TODO
        this.email = email,
        this.isActive = "t",
        this.Classes_IdClasses = Classes_IdClasses
    }
}

export const _addNewStudent = (newStudent, result) => {
    sql.query("INSERT INTO Students SET ?", newStudent, (err, res) => {
        if(err) {
            console.log("Error: " + err);
            result(err, null);
        } else {
            result(null, res);
        }
    })
}

export const _getAllStudents = (result) => {
    sql.query(`SELECT Students.idStudents, Students.firstName, Students.lastName, 
                Students.pesel, Students.age, Students.isActive, Students.email, Classes.Symbol as class
                FROM Students JOIN Classes on Students.Classes_IdClasses=Classes.idClasses`, (err, res) => {
        if(err) {
            console.log("Error: " + err);
            result(err, null)
        } else {
            result(null, res)
        }
    })
}

export const _getStudentById = (id, result) => {
    sql.query("SELECT * FROM Students WHERE Students.idStudents = ? LIMIT 1", id, (err, res) => {
        if(err) {
            console.log("Error: " + err);
            result(err, null)
        } else {
            result(null, res)
        }
    })
}
// TO FIX !!!
export const _updateStudent = (id, updatedStudent, result) => {
    sql.query("UPDATE Students SET ? WHERE Students.idStudents = ?"),
        [updatedStudent, id], (err, res) => {
            if(err){
                console.log("Error: " + err);
                result(err, null)
            } else {
                result(null, res)
            }
    }
}

export const _disableStudent = (id, result) => {
    sql.query("UPDATE Students SET `isActive` = 'f' WHERE Students.idStudents = ?", id, (err, res) => {
        if(err){
            console.log("Error: " + err);
            result(err, null)
        } else {
            result(null, res)
        }
    })
}

export const _getStudentGrades = (id, result) => {
    sql.query(`SELECT Grades.value, Grades.weight, Subjects.name as subject FROM Students 
               JOIN Grades 
               ON Students.idStudents=Grades.Students_idStudents 
               JOIN Subjects 
               ON Grades.Subjects_idSubjects=Subjects.idSubjects
               WHERE Students.idStudents = ?`, id, (err, res) => {
        if(err) {
            console.log("Error: " + err);
            result(err, null)
        } else {
            result(null, res)
        }
    })
}