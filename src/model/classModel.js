import { sql } from "../db.js";

export class Class {
    constructor(Profile, StartYear, Symbol) {
        this.Profile = Profile,
        this.StartYear = StartYear,
        this.Symbol = Symbol
    }
}

export const _getAllClasses = (result) => {
    sql.query("SELECT * FROM Classes", (err, res) => {
        if(err) {
            console.log("Error: " + err);
            result(err, null)
        } else {
            result(null, res)
        }
    })
}