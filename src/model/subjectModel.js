import { sql } from "../db.js";

export class Subject {
    constructor({Name, Category}) {
        this.Name = Name,
        this.Category = Category
    }
}

export const _getAllSubjects = (result) => {
    sql.query(`SELECT * FROM Subjects`, (err, res) => {
        if(err)
            result(err, null);
        else
            result(null, res);
    })
}