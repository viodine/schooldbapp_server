import { sql } from "../db.js";

export class Teacher {
    constructor({FirstName, LastName, Age, YearsOfExperience, Email, Phone}) {
        this.FirstName = FirstName,
        this.LastName = LastName,
        this.Age = Age,
        this.YearsOfExperience = YearsOfExperience,
        this.Email = Email,
        this.Phone = Phone
    }
}

export const _getAllTeachers = (result) => {
    sql.query("SELECT * FROM Teachers", (err, res) => {
        if(err) {
            console.log("Error: " + err)
            result(err, null)
        } else {
            result(null, res)
        }
    })
}

export const _addNewTeacher = (newTeacher, result) => {
    sql.query("INSERT INTO Teachers SET ?", newTeacher, (err, res) => {
        if (err) {
            console.log("Error: " + err)
            result(err, null)
        } else {
            result(null, res)
        }
    })
}

