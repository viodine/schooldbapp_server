import { sql } from "../db.js";

export class Grade {
    constructor({value, weight, Students_idStudents, Subjects_idSubjects}) {
        this.value = value,
        this.weight = weight,
        this.Subjects_idSubjects = Subjects_idSubjects,
        this.Students_idStudents = Students_idStudents
    }
}

export const _addNewGrade = (newGrade, result) => {
    sql.query("INSERT INTO Grades SET ?", newGrade, (err, res) => {
        if(err) {
            console.log("Error: " + err)
            result(err, null)
        } else {
            result(null, res)
        }
    })
}

export const _getSingleGrade = (gradeId, result) => {
    sql.query("SELECT * FROM Grades WHERE Grades.idGrades =?", gradeId, (err, res) => {
        if(err) {
            console.log("Error: " + err)
            result(err, null)
        } else {
            result(null, res)
        }
    })
}

export const _deleteGrade = (gradeId, result) => {
    sql.query("DELETE FROM Grades WHERE Grades.idGrades =?", gradeId, (err, res) => {
        if(err) {
            console.log("Error: " + err)
            result(err, null)
        } else {
            result(null, res)
        }
    })
}