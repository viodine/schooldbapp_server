import * as Teacher from "../model/teacherModel.js";

// Returns JSON with all teachers data.
export const getAllTeachers = (req, res) => {
    Teacher._getAllTeachers((err, result) => {
        if(err)
            res.send(err)
        res.json(result)
    })
}

// Takes JSON with teachers data and adds it to database.
export const addNewTeacher = (req, res) => {
    const newTeacher = new Teacher.Teacher(req.body)
    Teacher._addNewTeacher(newTeacher, (err, result) => {
        if(err)
            res.send(err)
        res.json(result)
    })
}