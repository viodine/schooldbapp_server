import * as Student from "../model/studentModel.js";

// Return JSON of all students with all informations.
export const getAllStudents = (req, res) => {
    Student._getAllStudents((err, result)=> {
        if(err)
            res.send(err);
        res.json(result);
    })
};

// Take JSON with student data and add it to database.
export const addNewStudent = (req, res) => {
    const newStudent = new Student.Student(req.body);
    if(!newStudent.firstName || !newStudent.lastName || 
        !newStudent.pesel || !newStudent.age ||
        !newStudent.gender || !newStudent.Classes_IdClasses) {
            res.status(400).send({error: true, message: "Please provide valid student data."})
    } else {
        Student._addNewStudent(newStudent, (err, result) => {
            if(err)
                res.send(err);
            res.json(result)
        })    
    }
};

// Returns JSON with single student's data.
export const getStudentById = (req, res) => {
    Student._getStudentById(req.params.id, (err, result) => {
        if(err)
            res.send(err)
        res.json(result)
    })
}

// Takes JSON with updated data and modify existing student.
export const updateStudent = (req, res) => {
    const updatedStudent = new Student.Student(req.body);
    Student._updateStudent(req.params.id, updatedStudent, (err, result) => {
        if(err)
            res.send(err)
        res.json(result)
    })
}

// Sets existing student state to inactive.
export const disableStudent = (req, res) => {
    Student._disableStudent(req.params.id, (err, result) => {
        if(err)
            res.send(err)
        res.json(result)
    })
}

// Returns JSON with single student's grades.
export const getStudentGrades = (req, res) => {
    Student._getStudentGrades(req.params.id, (err, result) => {
        if(err)
            res.send(err)
        res.json(result)
    })
}