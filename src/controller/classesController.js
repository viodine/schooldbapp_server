import * as Class from '../model/classModel.js';

// Return JSON of all classes with all informations.
export const getAllClasses = (req, res) => {
    Class._getAllClasses((err, result)=> {
        if(err)
            res.send(err);
        res.json(result);
    })
};