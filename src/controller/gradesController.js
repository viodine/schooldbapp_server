import * as Grade from "../model/gradeModel.js";

// Takes JSON with single grade data and bind it to student.
export const addNewGrade = (req, res) => {
    const newGrade = new Grade.Grade(req.body)
    Grade._addNewGrade(newGrade, (err, result) => {
        if(err)
            res.send(err)
        res.json(result)
    })
}

// Takes id of the grade and returns JSON with grade's data.
export const getSingleGrade = (req, res) => {
    Grade._getSingleGrade(req.params.gId, (err, result) => {
        if(err)
            res.send(err)
        res.json(result)
    })
}

// Takes id of the grade and deletes it from database.
export const deleteGrade = (req, res) => {
    Grade._deleteGrade(req.params.gId, (err, result) => {
        if(err)
            res.send(err)
        res.json(result)
    })
}