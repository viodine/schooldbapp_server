import * as Subject from '../model/subjectModel.js';

export const getAllSubjects = (req, res) => {
    Subject._getAllSubjects((err, result) => {
        if(err)
            res.send(err)
        res.json(result);
    })
}