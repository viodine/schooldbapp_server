import * as studentsController from "../controller/studentsController.js";
import * as gradesController from "../controller/gradesController.js";
import * as teachersController from "../controller/teachersController.js";
import * as classesController from "../controller/classesController.js";
import * as subjectsController from "../controller/subjectsController.js";

export const routes = (app) => {
    
// Student related routes
    app.route('/students')
        .get(studentsController.getAllStudents)
        .post(studentsController.addNewStudent);

    app.route('/students/:id')
        .get(studentsController.getStudentById)
        .put(studentsController.updateStudent)
        .delete(studentsController.disableStudent);

    app.route('/students/:id/grades')
        .get(studentsController.getStudentGrades)
        .post(gradesController.addNewGrade)

// Grades routes
    app.route('/students/:sId/grades/:gId')
        .get(gradesController.getSingleGrade)
        .delete(gradesController.deleteGrade)

// Teachers routes
    app.route('/teachers')
        .get(teachersController.getAllTeachers)
        .post(teachersController.addNewTeacher);

    // app.route('/teacher/:id')
    //     .get(teachersController.getTeacher)
    //     .put(teachersController.updateTeacher)
    //     .delete(teachersController.disableTeacher);

    // app.route('/teacher/:id/students')
    //     .get(teachersController.getStudentsByTeacher)

// Classes routes
    app.route('/classes')
        .get(classesController.getAllClasses)
    //     .post(classesController.addNewClass)
    
    // app.route('/classes/:id')
    //     .get(classesController.getClass)
    //     .post(classesController.addToClass)
    //     .put(classesController.updateClass)
    
// Subjects routes
    app.route('/subjects')
        .get(subjectsController.getAllSubjects)
    //     .post(subjectsController.addNewSubject)

    // app.route('/subjects/:id')
    //     .get(subjectsController.getSubject)
    //     .put(subjectsController.updateSubject)
    //     .delete(subjectsController.deleteSubject)
}