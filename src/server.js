import express from 'express';
import bodyParser from 'body-parser';
import mysql from 'mysql';
import { routes } from './routes/appRoutes.js';
var cors = require('cors');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'viodine',
    password: 'ViGilantE281192!',
    database: 'SchoolDB',
    port: 3306
});

db.connect();

const app = express();
const port = process.env.PORT || 3000;

app.listen(port);
console.log('API server started on: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
routes(app);