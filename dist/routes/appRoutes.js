"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.routes = void 0;

var studentsController = _interopRequireWildcard(require("../controller/studentsController.js"));

var gradesController = _interopRequireWildcard(require("../controller/gradesController.js"));

var teachersController = _interopRequireWildcard(require("../controller/teachersController.js"));

var classesController = _interopRequireWildcard(require("../controller/classesController.js"));

var subjectsController = _interopRequireWildcard(require("../controller/subjectsController.js"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

var routes = function routes(app) {
  // Student related routes
  app.route('/students').get(studentsController.getAllStudents).post(studentsController.addNewStudent);
  app.route('/students/:id').get(studentsController.getStudentById).put(studentsController.updateStudent)["delete"](studentsController.disableStudent);
  app.route('/students/:id/grades').get(studentsController.getStudentGrades).post(gradesController.addNewGrade); // Grades routes

  app.route('/students/:sId/grades/:gId').get(gradesController.getSingleGrade)["delete"](gradesController.deleteGrade); // Teachers routes

  app.route('/teachers').get(teachersController.getAllTeachers).post(teachersController.addNewTeacher); // app.route('/teacher/:id')
  //     .get(teachersController.getTeacher)
  //     .put(teachersController.updateTeacher)
  //     .delete(teachersController.disableTeacher);
  // app.route('/teacher/:id/students')
  //     .get(teachersController.getStudentsByTeacher)
  // Classes routes

  app.route('/classes').get(classesController.getAllClasses); //     .post(classesController.addNewClass)
  // app.route('/classes/:id')
  //     .get(classesController.getClass)
  //     .post(classesController.addToClass)
  //     .put(classesController.updateClass)
  // Subjects routes

  app.route('/subjects').get(subjectsController.getAllSubjects); //     .post(subjectsController.addNewSubject)
  // app.route('/subjects/:id')
  //     .get(subjectsController.getSubject)
  //     .put(subjectsController.updateSubject)
  //     .delete(subjectsController.deleteSubject)
};

exports.routes = routes;