"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._getAllSubjects = exports.Subject = void 0;

var _db = require("../db.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Subject = function Subject(_ref) {
  var Name = _ref.Name,
      Category = _ref.Category;

  _classCallCheck(this, Subject);

  this.Name = Name, this.Category = Category;
};

exports.Subject = Subject;

var _getAllSubjects = function _getAllSubjects(result) {
  _db.sql.query("SELECT * FROM Subjects", function (err, res) {
    if (err) result(err, null);else result(null, res);
  });
};

exports._getAllSubjects = _getAllSubjects;