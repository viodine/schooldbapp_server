"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._deleteGrade = exports._getSingleGrade = exports._addNewGrade = exports.Grade = void 0;

var _db = require("../db.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Grade = function Grade(_ref) {
  var value = _ref.value,
      weight = _ref.weight,
      Students_idStudents = _ref.Students_idStudents,
      Subjects_idSubjects = _ref.Subjects_idSubjects;

  _classCallCheck(this, Grade);

  this.value = value, this.weight = weight, this.Subjects_idSubjects = Subjects_idSubjects, this.Students_idStudents = Students_idStudents;
};

exports.Grade = Grade;

var _addNewGrade = function _addNewGrade(newGrade, result) {
  _db.sql.query("INSERT INTO Grades SET ?", newGrade, function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

exports._addNewGrade = _addNewGrade;

var _getSingleGrade = function _getSingleGrade(gradeId, result) {
  _db.sql.query("SELECT * FROM Grades WHERE Grades.idGrades =?", gradeId, function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

exports._getSingleGrade = _getSingleGrade;

var _deleteGrade = function _deleteGrade(gradeId, result) {
  _db.sql.query("DELETE FROM Grades WHERE Grades.idGrades =?", gradeId, function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

exports._deleteGrade = _deleteGrade;