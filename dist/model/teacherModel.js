"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._addNewTeacher = exports._getAllTeachers = exports.Teacher = void 0;

var _db = require("../db.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Teacher = function Teacher(_ref) {
  var FirstName = _ref.FirstName,
      LastName = _ref.LastName,
      Age = _ref.Age,
      YearsOfExperience = _ref.YearsOfExperience,
      Email = _ref.Email,
      Phone = _ref.Phone;

  _classCallCheck(this, Teacher);

  this.FirstName = FirstName, this.LastName = LastName, this.Age = Age, this.YearsOfExperience = YearsOfExperience, this.Email = Email, this.Phone = Phone;
};

exports.Teacher = Teacher;

var _getAllTeachers = function _getAllTeachers(result) {
  _db.sql.query("SELECT * FROM Teachers", function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

exports._getAllTeachers = _getAllTeachers;

var _addNewTeacher = function _addNewTeacher(newTeacher, result) {
  _db.sql.query("INSERT INTO Teachers SET ?", newTeacher, function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

exports._addNewTeacher = _addNewTeacher;