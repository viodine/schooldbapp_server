"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._getStudentGrades = exports._disableStudent = exports._updateStudent = exports._getStudentById = exports._getAllStudents = exports._addNewStudent = exports.Student = void 0;

var _db = require("../db.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Student = function Student(_ref) {
  var firstName = _ref.firstName,
      lastName = _ref.lastName,
      pesel = _ref.pesel,
      age = _ref.age,
      gender = _ref.gender,
      email = _ref.email,
      Classes_IdClasses = _ref.Classes_IdClasses;

  _classCallCheck(this, Student);

  this.firstName = firstName, this.lastName = lastName, this.pesel = pesel, // TODO
  this.age = age, // TODO
  this.gender = gender, // TODO
  this.email = email, this.isActive = "t", this.Classes_IdClasses = Classes_IdClasses;
};

exports.Student = Student;

var _addNewStudent = function _addNewStudent(newStudent, result) {
  _db.sql.query("INSERT INTO Students SET ?", newStudent, function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

exports._addNewStudent = _addNewStudent;

var _getAllStudents = function _getAllStudents(result) {
  _db.sql.query("SELECT Students.idStudents, Students.firstName, Students.lastName, \n                Students.pesel, Students.age, Students.isActive, Students.email, Classes.Symbol as class\n                FROM Students JOIN Classes on Students.Classes_IdClasses=Classes.idClasses", function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

exports._getAllStudents = _getAllStudents;

var _getStudentById = function _getStudentById(id, result) {
  _db.sql.query("SELECT * FROM Students WHERE Students.idStudents = ? LIMIT 1", id, function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
}; // TO FIX !!!


exports._getStudentById = _getStudentById;

var _updateStudent = function _updateStudent(id, updatedStudent, result) {
  _db.sql.query("UPDATE Students SET ? WHERE Students.idStudents = ?"), [updatedStudent, id], function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  };
};

exports._updateStudent = _updateStudent;

var _disableStudent = function _disableStudent(id, result) {
  _db.sql.query("UPDATE Students SET `isActive` = 'f' WHERE Students.idStudents = ?", id, function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

exports._disableStudent = _disableStudent;

var _getStudentGrades = function _getStudentGrades(id, result) {
  _db.sql.query("SELECT Grades.value, Grades.weight, Subjects.name as subject FROM Students \n               JOIN Grades \n               ON Students.idStudents=Grades.Students_idStudents \n               JOIN Subjects \n               ON Grades.Subjects_idSubjects=Subjects.idSubjects\n               WHERE Students.idStudents = ?", id, function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

exports._getStudentGrades = _getStudentGrades;