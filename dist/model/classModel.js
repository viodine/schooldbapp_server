"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._getAllClasses = exports.Class = void 0;

var _db = require("../db.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Class = function Class(Profile, StartYear, _Symbol) {
  _classCallCheck(this, Class);

  this.Profile = Profile, this.StartYear = StartYear, this.Symbol = _Symbol;
};

exports.Class = Class;

var _getAllClasses = function _getAllClasses(result) {
  _db.sql.query("SELECT * FROM Classes", function (err, res) {
    if (err) {
      console.log("Error: " + err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

exports._getAllClasses = _getAllClasses;