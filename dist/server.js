"use strict";

var _express = _interopRequireDefault(require("express"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _mysql = _interopRequireDefault(require("mysql"));

var _appRoutes = require("./routes/appRoutes.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var cors = require('cors');

var db = _mysql["default"].createConnection({
  host: 'localhost',
  user: 'viodine',
  password: 'ViGilantE281192!',
  database: 'SchoolDB',
  port: 3306
});

db.connect();
var app = (0, _express["default"])();
var port = process.env.PORT || 3000;
app.listen(port);
console.log('API server started on: ' + port);
app.use(_bodyParser["default"].urlencoded({
  extended: true
}));
app.use(_bodyParser["default"].json());
app.use(cors());
(0, _appRoutes.routes)(app);