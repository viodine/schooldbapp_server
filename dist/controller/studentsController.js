"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getStudentGrades = exports.disableStudent = exports.updateStudent = exports.getStudentById = exports.addNewStudent = exports.getAllStudents = void 0;

var Student = _interopRequireWildcard(require("../model/studentModel.js"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

// Return JSON of all students with all informations.
var getAllStudents = function getAllStudents(req, res) {
  Student._getAllStudents(function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
}; // Take JSON with student data and add it to database.


exports.getAllStudents = getAllStudents;

var addNewStudent = function addNewStudent(req, res) {
  var newStudent = new Student.Student(req.body);

  if (!newStudent.firstName || !newStudent.lastName || !newStudent.pesel || !newStudent.age || !newStudent.gender || !newStudent.Classes_IdClasses) {
    res.status(400).send({
      error: true,
      message: "Please provide valid student data."
    });
  } else {
    Student._addNewStudent(newStudent, function (err, result) {
      if (err) res.send(err);
      res.json(result);
    });
  }
}; // Returns JSON with single student's data.


exports.addNewStudent = addNewStudent;

var getStudentById = function getStudentById(req, res) {
  Student._getStudentById(req.params.id, function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
}; // Takes JSON with updated data and modify existing student.


exports.getStudentById = getStudentById;

var updateStudent = function updateStudent(req, res) {
  var updatedStudent = new Student.Student(req.body);

  Student._updateStudent(req.params.id, updatedStudent, function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
}; // Sets existing student state to inactive.


exports.updateStudent = updateStudent;

var disableStudent = function disableStudent(req, res) {
  Student._disableStudent(req.params.id, function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
}; // Returns JSON with single student's grades.


exports.disableStudent = disableStudent;

var getStudentGrades = function getStudentGrades(req, res) {
  Student._getStudentGrades(req.params.id, function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
};

exports.getStudentGrades = getStudentGrades;