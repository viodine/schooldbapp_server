"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteGrade = exports.getSingleGrade = exports.addNewGrade = void 0;

var Grade = _interopRequireWildcard(require("../model/gradeModel.js"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

// Takes JSON with single grade data and bind it to student.
var addNewGrade = function addNewGrade(req, res) {
  var newGrade = new Grade.Grade(req.body);

  Grade._addNewGrade(newGrade, function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
}; // Takes id of the grade and returns JSON with grade's data.


exports.addNewGrade = addNewGrade;

var getSingleGrade = function getSingleGrade(req, res) {
  Grade._getSingleGrade(req.params.gId, function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
}; // Takes id of the grade and deletes it from database.


exports.getSingleGrade = getSingleGrade;

var deleteGrade = function deleteGrade(req, res) {
  Grade._deleteGrade(req.params.gId, function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
};

exports.deleteGrade = deleteGrade;