"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addNewTeacher = exports.getAllTeachers = void 0;

var Teacher = _interopRequireWildcard(require("../model/teacherModel.js"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

// Returns JSON with all teachers data.
var getAllTeachers = function getAllTeachers(req, res) {
  Teacher._getAllTeachers(function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
}; // Takes JSON with teachers data and adds it to database.


exports.getAllTeachers = getAllTeachers;

var addNewTeacher = function addNewTeacher(req, res) {
  var newTeacher = new Teacher.Teacher(req.body);

  Teacher._addNewTeacher(newTeacher, function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
};

exports.addNewTeacher = addNewTeacher;