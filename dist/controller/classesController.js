"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllClasses = void 0;

var Class = _interopRequireWildcard(require("../model/classModel.js"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

// Return JSON of all classes with all informations.
var getAllClasses = function getAllClasses(req, res) {
  Class._getAllClasses(function (err, result) {
    if (err) res.send(err);
    res.json(result);
  });
};

exports.getAllClasses = getAllClasses;